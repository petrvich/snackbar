package cz.tmobile.cdcp.snackbar.backend.model.dto;

import lombok.Data;

@Data
public class PaymentDetailDto {

    private Integer prefix;
    private Integer number;
    private String code;
}
