package cz.tmobile.cdcp.snackbar.backend.service.impl;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import cz.tmobile.cdcp.snackbar.backend.model.dto.PaymentDetailDto;
import cz.tmobile.cdcp.snackbar.backend.service.GenerateQqCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Service
public class GenerateQqCodeServiceImpl implements GenerateQqCodeService {

    private Path generateQRCodeImage(String text, int width, int height) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        Path path = Files.createTempFile("QrCode", ".png");
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
        return path;
    }

    public Path generatePaymentQrCode(PaymentDetailDto dto) {

        return generateQRCodeImage("This is my first QR Code", 350, 350);
    }

}
